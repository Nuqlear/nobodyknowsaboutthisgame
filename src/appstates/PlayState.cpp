#include "PlayState.h"
#include "../core/GameEngine.h"
#include "../components/Text.h"
#include "../core/Graphics.h"
#include "../components/Texture.h"

#include <time.h>

const float PlayState::M2P = 20;
const float PlayState::P2M = 1/M2P;

PlayState::PlayState() {
    camerax = cameray = 0;
    world=new b2World(b2Vec2(0.0,9.81));
    world->SetAllowSleeping(true);
    addRect(GameEngine::inst()->getPixW()/2, GameEngine::inst()->getPixH() - 50, GameEngine::inst()->getPixW(), 100, false);
    timem = 0;
    srand(time(NULL));
}

PlayState::~PlayState() {
    delete world;
    for (auto *l : lights) {
        delete l;
    }
}

b2Body* PlayState::addRect(double x, double y, int w,int h,bool dyn) {
        b2BodyDef bodydef;
        bodydef.allowSleep = true;
        bodydef.position.Set(x*P2M,y*P2M);
        if(dyn)
            bodydef.type=b2_dynamicBody;
        b2Body* body=world->CreateBody(&bodydef);
       
        b2PolygonShape shape;
        shape.SetAsBox(P2M*w/2,P2M*h/2);

        b2FixtureDef fixturedef;
        fixturedef.shape=&shape;
        fixturedef.density=1.0;
        body->CreateFixture(&fixturedef);
        return body;
}

b2Body* PlayState::addCircle(double x, double y, int w, int h, bool dyn) {
        b2BodyDef bodydef;
        bodydef.position.Set(x*P2M, y*P2M);
        if(dyn)
            bodydef.type = b2_dynamicBody;
        b2Body* body = world->CreateBody(&bodydef);

        b2CircleShape shape;
        shape.m_p.Set(0, 0);
        shape.m_radius = P2M*w/2;

        b2FixtureDef fixturedef;
        fixturedef.shape = &shape;
        fixturedef.density = 1.0;
        body->CreateFixture(&fixturedef);
        return body;
}
 
void PlayState::render() {
    glTranslatef(-camerax, -cameray, 0);
    Graphics *g = Graphics::inst();

    // there should be background

    b2Body* tmp=world->GetBodyList();
    b2Vec2 points[4];
    while(tmp) {
        const b2Transform& xf = tmp->GetTransform();
        for (b2Fixture* f=tmp->GetFixtureList(); f; f=f->GetNext()) {
            switch (f->GetType()) {
                case b2Shape::e_circle: {
                    b2CircleShape* circle = (b2CircleShape*) f->GetShape();
                    b2Vec2 center = b2Mul(xf, circle->m_p);
                    float32 radius = circle->m_radius;
                    g->renderCircle(center, radius, M2P, 0, 0, 255, false);
                    tmp=tmp->GetNext();
                    break;
                }
                default: {
                    for (int i=0; i<4; i++)
                        points[i]=((b2PolygonShape*)tmp->GetFixtureList()->GetShape())->GetVertex(i);
                    if (tmp->GetType() == b2_dynamicBody)
                        g->renderRect(points, tmp->GetWorldCenter(), tmp->GetAngle(), M2P, 0, 200, 0);
                    else
                        g->renderRect(points, tmp->GetWorldCenter(), tmp->GetAngle(), M2P, 100, 100, 100);
                    tmp=tmp->GetNext();
                }
            }
        }
    }
    for (auto *l : lights) {
        l->render(camerax, cameray);
    }
}

void PlayState::processInput() {
    SDL_Event event;
    SDL_PollEvent(&event);
    keystate = SDL_GetKeyboardState(0);
    mousestate = GameEngine::inst()->getMouseState(&mousex, &mousey);
	if (event.type == SDL_KEYDOWN) {
		if (event.key.keysym.sym == SDLK_ESCAPE)
			GameEngine::inst()->changeState(GameEngine::states::Menu);
        if (event.key.keysym.sym == SDLK_r) {
            b2Body* node = world->GetBodyList();
            while (node) {
                b2Body* b = node; 
                node = node->GetNext();
                if (b->GetType() == b2_dynamicBody)
                    world->DestroyBody(b);
            }
            for (auto *l : lights) {
                delete l;
            }
            lights.clear();
        }
	}
    standardEventsHandling(event);
}

void PlayState::update(Uint16 dt) {
    if (mousestate) {
        if  (timem > 15) {
            if (mousestate & SDL_BUTTON(SDL_BUTTON_RIGHT))
                addRect(mousex + camerax, mousey + cameray, 20, 20, true);
            else if (mousestate & SDL_BUTTON(SDL_BUTTON_LEFT))
                lights.push_back(new Light(world, 
                                            M2P, 
                                            mousex + camerax,
                                            mousey + cameray,
                                            400,
                                            rand() % 100 + 155, 
                                            rand() % 100 + 155,
                                            rand() % 100 + 155));
            else if (mousestate & SDL_BUTTON(SDL_BUTTON_LEFT))
                //lastRect = addRect(mousex + camerax, mousey + cameray, 20, 20, 
                ;
            timem = 0;
        }
        else timem += 1;
    }
    if (keystate != nullptr) {
        int speed = 1.5;
        if (keystate[SDL_SCANCODE_LEFT])
            camerax-=speed * dt/4;
        if (keystate[SDL_SCANCODE_RIGHT])
            camerax+=speed * dt/4;
        if (keystate[SDL_SCANCODE_UP])
            cameray-=speed * dt/4;
        if (keystate[SDL_SCANCODE_DOWN])
            cameray+=speed * dt/4;
    }
    for (auto *l : lights) {
        l->update(camerax, cameray);
    }
    world->Step(1/60.0, 1, 1);
}

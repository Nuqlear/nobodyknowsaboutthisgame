#ifndef MENUSTATE_H_
#define MENUSTATE_H_

#include <glew.h>
#include <SDL.h>
#include <vector>

#include "AppState.h"
#include "../core/ResourcesManager.h"

class MenuItem;

class MenuState: public AppState {
private:
    unsigned char currentItem, prevItem;
    std::vector<MenuItem*> *items;
    void (*escapeAction)(MenuState*);
public:
    MenuState();
    virtual ~MenuState();
    void render();
    void processInput();
    void update(Uint16 dt);
    // Actions for MenuItems
    static void exit(MenuState*);
    static void start(MenuState*);
    static void continuee(MenuState*);
    static void stats(MenuState*);
    static void settings(MenuState*);
    static void backToMain(MenuState*);
    static void toggleFullscreen(MenuState*);
};

#endif /* MENUSTATE_H_ */

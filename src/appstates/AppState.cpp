#include "AppState.h"
#include "../core/ResourcesManager.h"
#include "../core/GameEngine.h"

bool AppState::running = false;

AppState::AppState() {
    resMgr = new ResourcesManager();
    keystate = nullptr;
    mousestate = 0;
}

AppState::~AppState() {
    delete resMgr;
}

void AppState::standardEventsHandling(SDL_Event event) {
    if(event.type == SDL_QUIT) {
        GameEngine::inst()->shutdown();
    }
}

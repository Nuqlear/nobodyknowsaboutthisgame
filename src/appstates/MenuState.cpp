#include "MenuState.h"
#include "../core/GameEngine.h"
#include "../components/MenuItem.h"

MenuState::MenuState() {
    prevItem = currentItem = 0;
    items = nullptr;
}

MenuState::~MenuState() {
    if (items != nullptr)
    for(MenuItem *item : *items) {
        delete item;
    }
    delete items;
}

void MenuState::render() {
    if (items != nullptr)
        for(MenuItem *item : *items) {
            item->render();
        }
}

void MenuState::processInput() {
    if (items == nullptr)
        backToMain(this);
    SDL_Event event;
    SDL_PollEvent(&event);
    int x, y ;
    int buttons = GameEngine::inst()->getMouseState(&x, &y);
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_RETURN) {
            items->at(currentItem)->activate(this);
            return;
        }
        else if (event.key.keysym.sym == SDLK_ESCAPE && escapeAction != nullptr) {
            escapeAction(this);
            return;
        }
        else if (event.key.keysym.sym == SDLK_DOWN) {
            items->at(currentItem)->setNotCurrent();
            currentItem = (currentItem + 1) % items->size();
            items->at(currentItem)->setCurrent();
            return;
        }
        else if (event.key.keysym.sym == SDLK_UP) {
            items->at(currentItem)->setNotCurrent();
            if (currentItem == 0)
                currentItem = items->size() - 1;
            else
                currentItem = (currentItem - 1);
            items->at(currentItem)->setCurrent();
            return;
        }
    }
    for(MenuItem *item : *items) {
        if(item->isMouseInside(x, y)) {
            if (items->at(currentItem) != item) {
                items->at(currentItem)->setNotCurrent();
                for (unsigned char i=0; i<items->size(); i++)
                    if (items->at(i) == item) {
                        currentItem = i;
                        break;
                    }
                item->setCurrent();
            }
        }
    }
    if ((buttons & SDL_BUTTON(1)) && items->at(currentItem)->isMouseInside(x, y))
        items->at(currentItem)->activate(this);
    standardEventsHandling(event);
}

void MenuState::update(Uint16 dt) {

}

void MenuState::exit(MenuState* ms) {
    GameEngine::inst()->shutdown();
}

void MenuState::start(MenuState *ms) {
    ms->escapeAction = start;
    ms->items->at(ms->currentItem)->setText("Continue");
    GameEngine::inst()->changeState(GameEngine::states::Play);
}

void MenuState::continuee(MenuState *ms) {
    ms->escapeAction = continuee;
    GameEngine::inst()->changeState(GameEngine::states::Play);
}

void MenuState::stats(MenuState *ms) {
    for(MenuItem *item : *(ms->items)) {
        delete item;
    }
    delete ms->items;
    ms->items = new std::vector<MenuItem*>();
    ms->prevItem = ms->currentItem;
    ms->currentItem = 0;
    ms->items->push_back(new MenuItem(100, 100, "Back", backToMain));
    ms->items->at(0)->setCurrent();
    ms->escapeAction = backToMain;
}

void MenuState::backToMain(MenuState *ms) {
    if (ms->items != nullptr) {
        for(MenuItem *item : *(ms->items)) {
            delete item;
        }
        delete ms->items;
    }
    ms->items = nullptr;
    ms->items = new std::vector<MenuItem*>();
    ms->currentItem = ms->prevItem;
    ms->items->push_back(new MenuItem(70, GameEngine::inst()->getPixH() - 200, "Start", start));
    ms->items->push_back(new MenuItem(70, GameEngine::inst()->getPixH() - 150, "Stats", stats));
    ms->items->push_back(new MenuItem(70, GameEngine::inst()->getPixH() - 100, "Exit", exit));
    ms->items->at(ms->currentItem)->setCurrent();
    ms->escapeAction = nullptr;
}
#ifndef APPSTATE_H_
#define APPSTATE_H_

#include "SDL.h"

class GameEngine;
class ResourcesManager;

class AppState {
protected:
    ResourcesManager *resMgr;
    const Uint8 * keystate;
    Uint32 mousestate;
    int mousex, mousey;
    void standardEventsHandling(SDL_Event);
public:
    AppState();
    static bool running;
    virtual void render() = 0;
    virtual void processInput() = 0;
    virtual void update(Uint16 dt) = 0;
    virtual ~AppState();
};

#endif /* APPSTATE_H_ */

#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <glew.h>
#include <math.h>
#include <SDL.h>
#include <queue>
#include <Box2D/Box2D.h>
#include <vector>

#include "AppState.h"
#include "../core/ResourcesManager.h"
#include "../components/Light.h"


class PlayState: public AppState {
private:
    std::vector<Light*> lights;
    unsigned int timem;
    Light *l;
    b2World* world;
    b2Body* ground;
    b2Body* addRect(double, double, int, int, bool);
    b2Body* addCircle(double, double, int, int, bool);
    static const float M2P;
    static const float P2M;
    float camerax, cameray;
    b2Body* lastRect;
public:
    PlayState();
    virtual ~PlayState();
    void processInput();
    void render();
    void update(Uint16 dt);
};

#endif /* PLAYSTATE_H */

#ifndef INTROSTATE_H_
#define INTROSTATE_H_

#include <glew.h>
#include <SDL.h>
#include <queue>

#include "AppState.h"
#include "../core/ResourcesManager.h"
#include "../components/Texture.h"

class IntroState: public AppState {
private:
    std::queue<Texture*> queue;
    Uint16 curTime;
    static const Uint16 maxTime = 5000;
    static const Uint16 fadeTime = 1500;
public:
    IntroState(std::vector<std::string>);
    virtual ~IntroState();
    void render();
    void processInput();
    void update(Uint16 dt);
};

#endif /* INTROSTATE_H_ */

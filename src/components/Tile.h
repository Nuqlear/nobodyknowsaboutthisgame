#ifndef TILE_H_
#define TILE_H_

#include <SDL.h>

class Texture;

class Tile {
protected:
    Uint16 t_offs_x, t_offs_y;
    Texture *texture;
public:
    Tile();
    Tile(Texture*, Uint16 = 0, Uint16 = 0);
    virtual ~Tile();
    virtual void render(double, double);
    const static unsigned char size = 16;
};

#endif /* TILE_H_ */

#include "Light.h"
#include "../core/Graphics.h"
#include "../core/GameEngine.h"


Light::Light(b2World *world_, float M2P_, double x_, double y_, double radius_, unsigned char r_, unsigned char g_, unsigned char b_) {
    point1.Set(x_, y_);
    radius = radius_;
    world = world_;
    M2P = M2P_;
    P2M = 1/M2P_;
    r = r_;
    g = g_;
    b = b_;
    values[0] = r/255.f;
    values[1] = g/255.f;
    values[2] = b/255.f;
    values[3] = 1.f;
    values[4] = radius;
    GLint compile_ok = GL_FALSE, link = GL_FALSE;
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source = 
    "uniform vec4 lightColor;"
    "uniform float radius;"
    "void main(void) {"
	"vec2 pos = vec2(gl_FragCoord.x, gl_FragCoord.y);"	
    "vec2 lightpos = vec2(radius, radius);"
	"float dist=distance(lightpos, pos);"
    "float attenuation=(2*radius)/(lightColor.x*dist+lightColor.y*dist+lightColor.z*dist*dist);"
	"gl_FragColor=vec4(attenuation,attenuation,attenuation,1.0)*lightColor;"
     "}";
    //ANOTHER SHADER
    //const char *fs_source = 
    //"uniform vec4 lightColor;"
    //"uniform float radius;"
    //"void main(void) {"
    //"vec2 pos = vec2(gl_FragCoord.x, gl_FragCoord.y);"
    //"float t = 1. - (distance(vec2(radius, radius), pos))/radius;"
    //"gl_FragColor = vec4(lightColor.x, lightColor.y, lightColor.z, lightColor.w) * t;"
    //"}";
    glShaderSource(fs, 1, &fs_source, NULL);
    glCompileShader(fs);
    glGetShaderiv(fs, GL_COMPILE_STATUS, &compile_ok);
    if (0 == compile_ok) {
        std::cerr << "glCompileShader error" << std::endl;
    }
    program = glCreateProgram();
    glAttachShader(program, fs);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &link);
    if (!link) {
        std::cerr << "glLinkProgram error" << std::endl;
    }
    glGenFramebuffersEXT(1, &fb);
    glGenTextures(1, &color_tex);
    glBindTexture(GL_TEXTURE_2D, color_tex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glGenRenderbuffersEXT(1, &depth_rb);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, radius*2, radius*2, 0,GL_RGBA, GL_INT, NULL);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,GL_COLOR_ATTACHMENT0_EXT,GL_TEXTURE_2D, color_tex, 0);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,GL_DEPTH_COMPONENT24, radius*2, radius*2);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,GL_DEPTH_ATTACHMENT_EXT,GL_RENDERBUFFER_EXT, depth_rb);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

Light::~Light(void) {
    glDeleteFramebuffersEXT(1, &fb);
    glDeleteRenderbuffersEXT(1, &depth_rb);
}

void Light::update(double camerax, double cameray) {
    double minus_x = - point1.x + radius + camerax;
    double minus_y = - point1.y + radius + cameray;
    points.clear();
    points.push_back(radius + camerax);
    points.push_back(radius + cameray);
    for(double i = 0; i < 2 * M_PI; i += M_PI / 50) {
        point2.Set(point1.x + radius*cos(i), point1.y + radius*sin(i));
        world->RayCast(&callback, b2Vec2(point1.x * P2M, point1.y  * P2M), b2Vec2(point2.x * P2M, point2.y * P2M));
        if (callback.m_fixture != 0) {
            points.push_back(callback.m_point.x * M2P + minus_x);
            points.push_back(callback.m_point.y * M2P + minus_y);
        }
        else {
            points.push_back(point2.x + minus_x);
            points.push_back(point2.y + minus_y);
        }
        callback.m_fixture = 0;
    }
}

void Light::render(double camerax, double cameray) {
    
    if ((point1.x - radius > GameEngine::inst()->getPixW() + camerax) || (point1.x + radius < camerax) ||
        (point1.y - radius > GameEngine::inst()->getPixH() + cameray) || (point1.y + radius < cameray))
        return;

    glUseProgram(program);
    GLint lightColor_ = glGetUniformLocation(program, "lightColor");
    glUniform4fv(lightColor_, 1, values);
    GLint radius_ = glGetUniformLocation(program, "radius");
    glUniform1fv(radius_, 1, &(values[4]));
    glPushAttrib(GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, radius*2, radius*2);
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, radius*2, radius*2, 0, -1, 1 );
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_tex, 0 );
    glClearColor(0, 0, 0, 1);
    glClear( GL_COLOR_BUFFER_BIT );
    glDisable(GL_BLEND);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(float), &points[0], GL_STATIC_DRAW);
    glVertexPointer(2, GL_FLOAT, 0, NULL);   
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDrawArrays(GL_TRIANGLE_FAN, 0, points.size()/2);

    glEnable(GL_BLEND);
    glUseProgram(0);
    glPopAttrib();
    glPopMatrix();
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    Graphics::inst()->renderImage(color_tex, point1.x - radius, point1.y - radius, radius*2, radius*2, true);
}
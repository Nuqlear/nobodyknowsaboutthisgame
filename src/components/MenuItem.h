#ifndef MENUITEM_H_
#define MENUITEM_H_

#include <string>
#include "Text.h"
#include "AbstractWidget.h"

class GameEngine;
class MenuState;

class MenuItem: public AbstractWidget {
private:
    void (*activate_)(MenuState*);
    Text text;
public:
    MenuItem(double, double, std::string, void(*)(MenuState*));
    virtual ~MenuItem();
    void setCurrent();
    void setNotCurrent();
    void render();
    void activate(MenuState*);
    void setText(std::string);
    void setPos(double, double);
    bool isMouseInside(double, double);
    MenuItem* copy();
};

#endif /* MENUITEM_H_ */

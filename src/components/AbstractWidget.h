#ifndef ABSTRACTWIDGET_H_
#define ABSTRACTWIDGET_H_

class AbstractWidget {
protected:
    double x, y;
public:
    AbstractWidget(void) {};
    virtual ~AbstractWidget(void) {};
    virtual bool isMouseInside(double, double) = 0;
    virtual void render() = 0;
};

#endif /* ABSTRACTWIDGET_H_ */
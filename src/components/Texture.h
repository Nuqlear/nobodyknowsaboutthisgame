#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <iostream>

#include "../components/Resource.h"

class ResourcesManager;

class Texture: public Resource {
    friend ResourcesManager;
private:
    unsigned int textureid;
    int origw, origh;
    Texture(ResourcesManager* const resmgr) : Resource(resmgr) { origw = origh = textureid = 0; };
    ~Texture(void);
    void load(const std::string) override;
public:
    void render(double, double, int offs_x = 0, int offs_y = 0, int w = 0, int h = 0);
    void render2(double x, double y, int w_, int h_, int r, int g, int b);
};

#endif /* TEXTURE_H_ */

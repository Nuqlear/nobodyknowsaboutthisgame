#include "Texture.h"
#include "../core/Graphics.h"

Texture::~Texture() {
    Graphics::inst()->deleteTexture(textureid);
}

void Texture::load(const std::string  filename) {
    Graphics::inst()->load(filename, textureid, origw, origh);
}

void Texture::render(double x, double y, int offs_x, int offs_y, int w_, int h_) {
    unsigned int w, h;
    w = w_;
    h = h_;
    if (w_ == 0)
        w = origw;
    if (h_ == 0)
        h = origh;

    if (w_ > origw || h_ > origh)
        std::cerr << "crop more than original size. x=" << x << " y=" << y << std::endl;

    Graphics::inst()->renderImagePart(textureid, x, y, offs_x, offs_y, w, h, origw, origh);
}

// pos in center and texture resized
void Texture::render2(double x, double y, int w_, int h_, int r, int g, int b) {
    Graphics::inst()->renderImage(textureid, x, y, w_, h_);
}
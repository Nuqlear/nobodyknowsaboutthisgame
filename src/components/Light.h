#ifndef LIGHT_H_
#define LIGHT_H_

#include <Box2D/Box2D.h>
#include <vector>
#include <glew.h>

#include "../components/Texture.h"

class MyRayCastCallback: public b2RayCastCallback {
public:
    MyRayCastCallback() {
        m_fixture = NULL;
    }
    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point,const b2Vec2& normal, float32 fraction) {
        m_fixture = fixture;
        m_point = point;
        m_normal = normal;
        m_fraction = fraction;
        return fraction;
    }
    b2Fixture* m_fixture;
    b2Vec2 m_point;
    b2Vec2 m_normal;
    float32 m_fraction;
};

class Light {
private:
    b2World *world;
    float M2P, P2M;
    unsigned char r, g, b;
    float values[9];
    GLuint program;
    GLuint fb, color_tex, depth_rb;
    double radius;
    b2Vec2 point1;
    std::vector<float> points;
    b2Vec2 point2;
    MyRayCastCallback callback;
public:
    Light(b2World *world_, float M2P_, double x_, double y_, double radius_, unsigned char r_, unsigned char g_, unsigned char b_);
    ~Light(void);
    void update(double camerax, double cameray);
    void render(double, double);
};

#endif /* LIGHT_H_ */
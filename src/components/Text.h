#ifndef TEXT_H_
#define TEXT_H_

#include <glew.h>
#include "SDL_ttf.h"
#include <string>
#include <iostream>

class Text {
private:
    std::string text;
    std::string fontfilename;
    GLuint textureid;
    Uint16 fontsize;
    Uint16 w, h;
    SDL_Color color;
public:
    void setFont(std::string filename);
    void setText(std::string text);
    std::string getText();
    void setSize(Uint16 size);
    void setColor(unsigned char, unsigned char, unsigned char);
    void render(double X, double Y);
    void upd();
    Uint16 getW(), getH();
    Text();
    ~Text();
};
#endif /* TEXT_H_ */

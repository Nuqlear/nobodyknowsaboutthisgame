#include <glew.h>
#include "GameEngine.h"

int main(int argc, char* argv[]){
    GameEngine::inst()->run();
    return EXIT_SUCCESS;
}
#include <fstream>

#include "GameEngine.h"
#include "../appstates/AppState.h"
#include "../appstates/IntroState.h"
#include "../appstates/PlayState.h"
#include "../appstates/MenuState.h"
#include "../core/Graphics.h"

GameEngine *GameEngine::instance = nullptr;
const std::string GameEngine::config_path = "./rsc/config";

GameEngine *GameEngine::inst() {
    if (instance == nullptr) {
        instance = new GameEngine();
    }
    return instance;
}

GameEngine::GameEngine() {
    gamename = "SDL2/Opengl/Box2D";
    freopen("std.log", "w", stdout);
    freopen("err.log", "w", stderr);
    load();
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        std::cerr << "unable to init SDL: " << SDL_GetError() << std::endl;
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GetDesktopDisplayMode(0, &displayMode);
    scw = displayMode.w;
    sch = displayMode.h;
    if (fullscreen)
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, scw, sch, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
    else
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, pxw,pxh, SDL_WINDOW_OPENGL);
    glcontext = SDL_GL_CreateContext(window);
    if(fullscreen)
        glViewport( 0, 0, scw, sch);
    else
        glViewport( 0, 0, pxw, pxh );
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0, pxw, pxh, 0, -1, 1 );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    Graphics::inst();
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    TTF_Init();
    atexit(SDL_Quit);
    atexit(IMG_Quit);
    atexit(TTF_Quit);
    fps = new Text();
    showFps = true;
}

void GameEngine::init() {
    std::vector<std::string> v;
    v.push_back("rsc//1.jpg");
    v.push_back("rsc//1.jpg");
    v.push_back("rsc//1.jpg");
    introState = new IntroState(v);
    menuState = new MenuState();
    playState = new PlayState();
    currentState = menuState;
}

GameEngine::~GameEngine() {
    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
    delete introState;
    delete menuState;
    delete playState;
    delete fps;
}

void GameEngine::changeState(states state) {
    switch (state) {
    case Intro:
        currentState = introState;
        return;
    case Menu:
    	currentState = menuState;
    	return;
    case Play:
        currentState = playState;
        return;
    }
}

void GameEngine::render() {
    Graphics::inst()->clearScreen();
	currentState->render();
    if (showFps) {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        fps->render(0, 0);
    }
	SDL_GL_SwapWindow(window);
}

void GameEngine::run()  {
    Uint32 t = 0.0;
    const Uint32 dt = 10;
    Uint32 currentTime = SDL_GetTicks();
    Uint32 accumulator = 0.0;
    instance->init();
    currentState->running = true;
    unsigned int fpsCount = 0;
    Uint32 lastFpsTime = 0;
    while(currentState->running) {
        Uint32 newTime = SDL_GetTicks();
        Uint32 frameTime = newTime - currentTime;
        currentTime = newTime;
        accumulator += frameTime;
        while ( accumulator >= dt )
        {
            currentState->processInput();
            currentState->update(dt);
            accumulator -= dt;
        }
        render();
        if (showFps) {
            if ((currentTime - lastFpsTime) > 1000) {
                lastFpsTime = currentTime;
                std::stringstream out;
                out << "FPS: " << fpsCount;
                fps->setText(out.str());
                fps->upd();
                fpsCount = 0;
            }
            fpsCount ++;
        }
    }
}

void GameEngine::shutdown() {
	currentState->running = false;
}

void GameEngine::load() {
    ifstream config(this->config_path);
    if (!config) {
        std::cerr << "Unable to open config" << config_path << std::endl;
        pxw = 640;
        pxh = 480;
        fullscreen = false;
    }
    config >> pxw;
    config >> pxh;
    config >> fullscreen;
    config.close();
}

Uint32 GameEngine::getMouseState(int *x, int *y) {
    Uint32 mousestate = SDL_GetMouseState(x, y);
    if (fullscreen) {
        *x = (*x)*pxw/scw;
        *y = (*y)*pxh/sch;
    }
    return mousestate;
}

Uint16 GameEngine::getPixW() {
    return pxw;
}

Uint16 GameEngine::getPixH() {
    return pxh;
}

void GameEngine::save() {
    ofstream config(this->config_path);
    config << pxw << std::endl;
    config << pxh << std::endl;
    config << fullscreen << std::endl;
    config.close();
}
#include "ResourcesManager.h"

ResourcesManager::ResourcesManager() {
    push();
}

ResourcesManager::~ResourcesManager() {
    pop();
}

void ResourcesManager::push() {
    resources.push_back(unordered_map<std::string, Resource *>());
}

void ResourcesManager::pop() {
    unordered_map<std::string, Resource *> &v = resources[resources.size() - 1];
    unordered_map<std::string, Resource *>::iterator iter;
    for (iter = v.begin(); iter != v.end(); ++iter)
        delete iter->second;
    resources.pop_back();
}

#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <string>
#include <glew.h>
#include <SDL.h>
#include <SDL_image.h>
#include <Box2D/Box2D.h>

class Graphics {
private:
    static Graphics *instance;
    Graphics();
    virtual ~Graphics();
public:
    static Graphics *inst();
    double getLineLength(b2Vec2, b2Vec2);
    void renderLine(double, double, double, double, unsigned char, unsigned char, unsigned char);
    void renderRect(double, double, double, double, unsigned char, unsigned char, unsigned char);
    void renderFillRect(double, double, double, double, unsigned char, unsigned char, unsigned char);
    void renderCircle(float x, float y, float R, int n, unsigned char r, unsigned char g, unsigned char b);
    void renderRect(b2Vec2* points, b2Vec2 center, float32 angle, float M2P, unsigned char r = 255, unsigned char g = 255, unsigned char b = 255, bool fill = true);
    void renderCircle(b2Vec2 center, float32 radius, float M2P, unsigned char r = 255, unsigned char g = 255, unsigned char b = 255, bool fill = true);
    void renderImage(GLuint id, double x, double y, int w, int h, bool inverty=false);
    void renderImagePart(GLuint textureid, double x, double y, int offs_x, int offs_y, int w_, int h_, int origw, int origh, int r=255, int g=255, int b=255);
    void clearScreen();
    void deleteTexture(GLuint &textureid);
    void load(std::string, GLuint&, int&, int&);
};

#endif /* GRAPHICS_H_ */

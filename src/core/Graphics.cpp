#include "Graphics.h"

Graphics *Graphics::instance = nullptr;

Graphics::Graphics() {
    glClearColor (0, 0, 0, 0);
    glShadeModel( GL_SMOOTH );
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
    glewInit();
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

}

Graphics::~Graphics() {
}

Graphics* Graphics::inst() {
    if (instance == nullptr) {
        instance = new Graphics();
    }
    return instance;
}

void Graphics::renderLine(double x1, double y1, double x2, double y2, unsigned char r, unsigned char g, unsigned char b) {
    glBegin(GL_LINES);
        glColor3f(r/255.f, g/255.f, b/255.f);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
    glEnd();
}

void Graphics::renderFillRect(double x, double y, double w, double h, unsigned char r, unsigned char g, unsigned char b) {
    glBegin(GL_QUADS);
        glColor3f(r/255.f, g/255.f, b/255.f);
        //Bottom-left vertex (corner)
        glVertex3f(x-w/2, y-h/2, 0.0f );
        //Bottom-right vertex (corner)
        glVertex3f(x+w/2, y-h/2, 0.f );
        //Top-right vertex (corner)
        glVertex3f(x+w/2, y+h/2, 0.f );
        //Top-left vertex (corner)
        glVertex3f(x-w/2, y+h/2, 0.f );
    glEnd();
}

void Graphics::renderRect(double x, double y, double w, double h, unsigned char r, unsigned char g, unsigned char b) {
    glBegin(GL_QUADS);
        glColor3f(r/255.f, g/255.f, b/255.f);
        //Bottom-left vertex (corner)
        glVertex3f(x-w/2, y-h/2, 0.0f );
        //Bottom-right vertex (corner)
        glVertex3f(x+w/2, y-h/2, 0.f );
        //Top-right vertex (corner)
        glVertex3f(x+w/2, y+h/2, 0.f );
        //Top-left vertex (corner)
        glVertex3f(x-w/2, y+h/2, 0.f );
    glEnd();
}

void Graphics::clearScreen() {
    glClearColor(0.1, 0.1, 0.1, 1);
    glClear(GL_COLOR_BUFFER_BIT );
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Graphics::load(std::string filename, GLuint &textureid, int &origw, int &origh) {
    SDL_Surface *surface;
    int mode;
    if (!(surface = IMG_Load(filename.c_str())) ) {
        //std::cerr << "couldnt load image file '" << filename.c_str() << "'" << std::endl;
    }
    if (surface->format->BytesPerPixel == 3) {
        if(surface->format->Rmask==0x000000ff)
            mode = GL_RGB;
        else
            mode = GL_BGR;
    }
    else if (surface->format->BytesPerPixel == 4) { // RGBA 32bit
        if(surface->format->Rmask==0x000000ff)
            mode = GL_RGBA;
        else
            mode = GL_BGRA;
    }
    glGenTextures(1, &textureid);
    glBindTexture(GL_TEXTURE_2D, textureid);

    origw = surface->w;
    origh = surface->h;

    // this reads from the sdl surface and puts it into an opengl texture
    glTexImage2D(GL_TEXTURE_2D, 0, mode, surface->w, surface->h, 0, mode, GL_UNSIGNED_BYTE, surface->pixels);

    // these affect how this texture is drawn later on...
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

    SDL_FreeSurface(surface);
}

// pos in center of image; image will be rescaled;
void Graphics::renderImage(GLuint textureid, double x, double y, int w_, int h_, bool inverty) {
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScaled(1/double(w_), 1/double(h_), 1);

    glBindTexture(GL_TEXTURE_2D, textureid);

    glEnable(GL_TEXTURE_2D);
    // make a rectangle

    glBegin(GL_QUADS);

    if (!inverty) {
        // top left
        glTexCoord2f(0, 0);
        glVertex3f(x, y, 0);

        // top right
        glTexCoord2f(w_, 0);
        glVertex3f(x + w_, y, 0);

        // bottom right
        glTexCoord2i(w_, h_);
        glVertex3i(x + w_, y + h_, 0);

        // bottom left
        glTexCoord2i(0, h_);
        glVertex3i(x, y + h_, 0);
    }
    else {
        // top left
        glTexCoord2f(0, 0);
        glVertex3i(x, y + h_, 0); 

        // top right
        glTexCoord2f(w_, 0);
        glVertex3i(x + w_, y + h_, 0);

        // bottom right
        glTexCoord2i(w_, h_);
        glVertex3f(x + w_, y, 0);

        // bottom left
        glTexCoord2i(0, h_);
        glVertex3f(x, y, 0);  
    }

    glEnd();

    glDisable(GL_TEXTURE_2D );
}

// pos in top left corner; part of image; no resize;
void Graphics::renderImagePart(GLuint textureid, double x, double y, int offs_x, int offs_y, int w_, int h_, int origw, int origh, int r, int g, int b) {
    glColor4f(r/255.f, g/255.f, b/255.f, 1.f);
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScaled(1/double(origw), 1/double(origh), 1);

    glBindTexture(GL_TEXTURE_2D, textureid);

    glEnable(GL_TEXTURE_2D);

    glBegin(GL_QUADS);

    // top left
    glTexCoord2f(offs_x, offs_y);
    glVertex3f(x, y, 0);

    // top right
    glTexCoord2f(offs_x + w_, offs_y);
    glVertex3f(x + w_, y, 0);

    // bottom right
    glTexCoord2i(offs_x + w_, offs_y + h_);
    glVertex3i(x + w_, y + h_, 0);

    // bottom left
    glTexCoord2i(offs_x, offs_y + h_);
    glVertex3i(x, y + h_, 0);

    glEnd();

    glDisable(GL_TEXTURE_2D );
}

void Graphics::deleteTexture(GLuint &textureid) {
    glDeleteTextures(1, &textureid);
}

void Graphics::renderCircle(float x, float y, float R, int n, unsigned char r, unsigned char g, unsigned char b) {
    glColor3f(r/255.f, g/255.f, b/255.f);
    glBegin(GL_POLYGON);
    for(float i = 0; i < 2*3.14; i+= (2*3.14)/n){
        glVertex2f(x+cosf(i)*R, y+sinf(i)*R);
    }
    glEnd();
}

void Graphics::renderRect(b2Vec2* points, b2Vec2 center, float32 angle, float M2P, unsigned char r, unsigned char g, unsigned char b, bool fill) {
    glColor3f(r/255.f, g/255.f, b/255.f);
    glPushMatrix();
        glTranslatef(center.x*M2P, center.y*M2P, 0);
        glRotatef(angle*180.0/M_PI, 0, 0, 1);
        fill ? glBegin(GL_TRIANGLE_FAN) : glBegin(GL_LINE_LOOP);
            for(int i=0; i<4; i++)
                    glVertex2f(points[i].x*M2P, points[i].y*M2P);
        glEnd();
    glPopMatrix();
}

void Graphics::renderCircle(b2Vec2 center, float32 radius, float M2P, unsigned char r, unsigned char g, unsigned char b, bool fill) {
    glColor3f(r/255.f, g/255.f, b/255.f);
    glPushMatrix();
        glTranslatef(center.x*M2P,center.y*M2P,0);
        fill ? glBegin(GL_TRIANGLE_FAN) : glBegin(GL_LINE_LOOP);
		    for(double i = 0; i < 2 * M_PI; i += M_PI / 6)
 			    glVertex2f(cos(i) * radius * M2P, sin(i) * radius * M2P);
        glEnd();
    glPopMatrix();
}

double Graphics::getLineLength(b2Vec2 p1, b2Vec2 p2) {
    return sqrt(pow((p1.x - p2.x), 2) + pow(p1.y - p2.y, 2));
}
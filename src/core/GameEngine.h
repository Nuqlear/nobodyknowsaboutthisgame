#ifndef GAMEENGINE_H_
#define GAMEENGINE_H_

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <sstream>

#include "../components/Text.h"

class AppState;
class IntroState;
class MenuState;
class PlayState;

class GameEngine {
private:
    AppState *currentState;
    IntroState *introState;
    MenuState *menuState;
    PlayState *playState;
    Uint32 curr_ticks, prev_ticks;
    void render();
    static GameEngine *instance;
    void init();
    Uint16 pxw;
    Uint16 pxh;
    Uint16 scw;
    Uint16 sch;
    bool fullscreen;
    void save();
    void load();
    std::string gamename;
    SDL_Window *window;
    SDL_DisplayMode displayMode;
    GameEngine();
    Text *fps;
    bool showFps;
    virtual ~GameEngine();
    static const std::string config_path;
public:
    SDL_GLContext glcontext;
    Uint16 getPixW();
    Uint16 getPixH();
    Uint32 getMouseState(int*, int*);
    SDL_Window * getWindow();

    static GameEngine *inst();
    enum states {Intro, Menu, Play};
    void changeState(states);
    void run();
    void shutdown();
};

#endif /* GAMEENGINE_H_ */
